#!/bin/bash

TIMESTAMP=$(date +%Y%m%d%H%M)
CSV_FILE=search_results_$TIMESTAMP.csv
# CONTAINERID=$(docker container ls -a | grep postgres | cut -d' ' -f1)
CMD_SQL="\'copy search_results to '/tmp/$CSV_FILE'  DELIMITER ',' CSV HEADER\'"

# BKP_FILE=/tmp/search_results_${TIMESTAMP}.csv

# echo timestamp=$TIMESTAMP
# echo containerid=$CONTAINERID
# psql -h localhost -p 5432 -U postgres -d googlescraper_dev -c "copy search_results to '/tmp/search_results_${TIMESTAMP}.csv'  DELIMITER ',' CSV HEADER"
# echo "Exporting table to $BKP_FILE"
# docker cp $CONTAINERID:$BKP_FILE .
# echo 'Done'

ssh -t linuxadmin@168.205.92.81 "docker exec -u 0 $(docker container ls -a | grep postgres | cut -d ' ' -f1) 'psql -U postgres -d googlescraper_dev -c $CMD_SQL'"
ssh -t linuxadmin@168.205.92.81 "docker exec -u 0 $(docker container ls -a | grep postgres | cut -d ' ' -f1) 'mv /tmp/$CSV_FILE /tmp/bkpdb/'"