#!/bin/bash

TIMESTAMP=$(date +%Y%m%d%H%M)
CONTAINERID=$(docker container ls -a | grep postgres | cut -d' ' -f1)
BKP_FILE=/tmp/search_results_${TIMESTAMP}.csv

echo timestamp=$TIMESTAMP
echo containerid=$CONTAINERID
psql -h localhost -p 5432 -U postgres -d googlescraper_dev -c "copy search_results to '/tmp/search_results_${TIMESTAMP}.csv'  DELIMITER ',' CSV HEADER"
echo "Exporting table to $BKP_FILE"
docker cp $CONTAINERID:$BKP_FILE .
echo 'Done'