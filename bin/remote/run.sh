#!/bin/bash

# DEFAULTS: sudo bin/run.sh -u input/2019.Argentina/users-prod.csv -d $(pwd)

function check_args {
	if [ -z $1 ] || [ -z $2 ]; then
		usage
		echo 
	fi
}

function run_interactive {
	echo 'Running container in interactive mode... '$@
	check_args $@
	docker run --rm -e "users=$1" -v ${2}/output:/app/output -v ${2}/input:/app/input -it alegomes/google-search-trainer /bin/bash	
}

function run_daemon {
	echo 'Running container in daemon mode...' $@
	check_args $@
	docker run -e "users=$1" -v ${2}/output:/app/output -v ${2}/input:/app/input -d alegomes/google-search-trainer	
}

function usage {
	echo
	echo "Usage: $0 [-i] -u <users.csv> -d <dir>"
	echo
	echo "   -i For interactive mode"
	echo "   -u Users csv file (users_test.csv or users_prod.csv)"
	echo "   -d Local base dir to mount in the cotainer"
	echo
	echo " e.g. When running locally:       $0 -u input/users-test.csv -d $(pwd)"
	echo "      When running interactively: $0 -u input/users-test.csv -d $(pwd) -i"
	echo "      When running remotelly:     $0 -u input/users-prod.csv -d /home/linuxadmin"
	echo
	# exit -1 
}

###### Start

USERS=input/2019.Argentina/users-test.csv
DIR=$(pwd)

if [ $# -lt 1 ]; then
	usage	
fi

home=$1

while getopts "iu:d:" OPT
do
  case $OPT in
    i) 
		INTERTACTIVE=True
		# run_interactive $OPTARG
		;;
	u) 
		USERS=$OPTARG
		;;
	d)
		DIR=$OPTARG
		;;

	\?) 
		usage
		;;
  esac
done

# shift $((OPTIND - 1))
# ARG1=${@:$OPTIND:1}
# ARG2=${@:$OPTIND+1:1}


if [ -z $USERS ]; then
	# echo "-u option required."
	# usage
	echo "Using default value..."
		echo "  -u $USERS"
fi

if [ -z $DIR ]; then
	# echo "-d option required."
	# usage
	echo "Using default value..."
		echo "  -d $DIR"
fi

if [ -z $INTERACTIVE ]; then
	run_daemon $USERS $DIR
else
	run_interactive $USERS $DIR
fi







# home=/home/linuxadmin; docker run -v output:${home}/output -v input:${home}/input,readonly -it alegomes/google-search-trainer /bin/bash