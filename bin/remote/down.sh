#!/bin/bash

export NODE_ENV=production
docker stop $(docker container ls -a | grep trainer | grep train.sh | grep Up | cut -d ' ' -f1)